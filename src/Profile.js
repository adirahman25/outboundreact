import React, { Component } from 'react';


import { Container, Row, Col,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem  } from 'reactstrap';

import {BrowserRouter, Switch, Route } from 'react-router-dom';

import About from './About';
import Home from './App';
import Left from './img/1.jpeg'
import Center from './img/1.jpeg'
import Right from './img/1.jpeg'

import './App.css';

class Profile extends Component {
 
  render() {
    return (
      <BrowserRouter>
      <Switch>
        <Route  exact path="/" component={Home}/>
        <Route  path="/profile" component={Content}/>
        <Route  path="/about" component={About}/>
      </Switch>
      </BrowserRouter>
      
    );
  }
}

export default Profile;

export class Header extends Component {

  constructor(props){
    super(props)
    this.state = {
     
      isOpen: false
    }
    this.toggle = this.toggle.bind(this);
  }
  toggle(){
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render(){
    return (
      <Container-Fluid>
         <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Example</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink  href="/profile">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/about">About Us</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Artikel
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Outbound itu lucu
                  </DropdownItem>
                  <DropdownItem>
                    Outbound itu bercerita
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Berita
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
       
       <Container>
         {this.props.children}
       </Container>

  

      </Container-Fluid>
    );  
  }
}

class Content extends Component {

  render(){
    return (

      <Header> 

        <h1 class="text-center">Lorem Ipsum</h1>
        <p class="text-justify">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
        <Row>
          <Col xs="6">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </Col>
          <Col xs="6">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        
          </Col>
          
        </Row>
        
        <Row id="setMargin">
          <Col xs="4">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </Col>
          <Col xs="4">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </Col>
          <Col xs="4">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </Col>
        </Row>

        <Row id="setMargin">
            <Col xs="4">
              <img src={Left} className="img-fluid"/>
            </Col>
            <Col xs="4">
              <img src={Center}  className="img-fluid"/>
            </Col>
            <Col xs="4">
              <img src={Right}  className="img-fluid"/>
            </Col>  
        </Row>


      </Header>
    )
  }

}

