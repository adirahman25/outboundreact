import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Profile from './Profile';
import About from './About';
import {BrowserRouter, Switch, Route } from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <Switch>
        <Route  exact path="/" component={Home}/>
        <Route  path="/profile" component={Profile}/>
        <Route  path="/about" component={About}/>
      </Switch>
      </BrowserRouter>
    );
  }
}

export default App;

class Home extends Component {
  render() {
    return (
      <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="btn btn-success"
          href="/profile"
          target="_blank"
          rel="noopener noreferrer"
        >
          Enter
        </a>
      </header>
    </div>
    );
  }
}